"""
ReSMT
====================================
A package containing definitions of datatypes (some of which provide interfaces to Z3 functionalities), \
Z3 routines and some IO utilities. This code originated from a prototypical set of developments for the \
paper https://arxiv.org/abs/2003.11010 (N. Behr, M. Ghaffari Saadat and R. Heckel, 2020).

* Author: N. Behr

"""
