Welcome to ReSMT's documentation!
=================================

This ``Python 3`` library comprises a collection of algorithms realizing an implementation of \
*compositional rewriting theories* (in the sense of [#f0]_) based upon a "translation-based" approach \
(in the sense of [#f1]_), and is part of an \
ongoing work in progress with M. Ghaffari Saadat and R. Heckel (U Leicester). The main motivation for this \
work consisted in the desire to experiment with the \
`Microsoft Z3 Theorem Prover <https://github.com/Z3Prover/z3>`_ as a computational core for rewriting-theoretic \
algorithms, with the "translation-based" approach consisting in translating both categorical data structures \
as well as the rewriting-theoretic concepts into a format accessible to ``Z3``. For further conceptual \
and technical details on this work, please refer to our recent preprint \
*"Efficient Computation of Graph Overlaps for Rule Composition: Theory and Z3 Prototyping"* [#f2]_.

.. [#f0] N. Behr, "Tracelets and Tracelet Analysis Of Compositional Rewriting Systems" (accepted for \
   `ACT 2019 <http://www.cs.ox.ac.uk/ACT2019/>`_ in Oxford), 2019, \
   arXiv: `1904.12829 [cs.LO] <https://arxiv.org/abs/1904.12829>`_.

.. [#f1] R. Heckel, L. Lambers and M.G. Saadat, \
   “Analysis of Graph Transformation Systems: Native Vs Translation-Based Techniques.”, \
   `Electronic Proceedings in Theoretical Computer Science 309 (2019) \
   <https://arxiv.org/abs/1912.09607>`_, pp. 1–22.

.. [#f2] N. Behr, M. Ghaffari Saadat and R. Heckel, "Efficient Computation of Graph Overlaps for Rule \
   Composition: Theory and Z3 Prototyping" (contained in the \
   `pre-proceedings <https://sites.google.com/view/gcm2020/preproc>`_ of the  \
   `GCM 2020: Eleventh International Workshop \
   on Graph Computation Models <https://sites.google.com/view/gcm2020/>`_)

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   usage/Installation
   py_and_ipynb_examples/index
   API-reference/Reference

   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
