{
  "cells": [
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "%matplotlib inline\nfrom IPython.display import SVG, display, Image"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\nGCM 2020 supplementary information\n==================================\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The present set of examples and materials is intended to serve as a supplementary information for the \\\nconference paper *Efficient Computation of Overlaps for Rule Composition: Theory and Z3 Prototyping* \\\n(Behr et al. 2020).\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Preliminaries: setting up the Jupyter environment\n-------------------------------------------------\n\nIf you have not already done so, please consult the **Installation instructions** chapter of the ``ReSMT`` \\\ndocumentation for  information on how to install the necessary Python version, Python packages and \\\nthe necessary additional shell scripts. You  will also require a local copy of the \\\n**ReSMT GitLab repository**, which may be obtained as follows: ::\n\n    $ git clone https://gitlab.com/nicolasbehr/ReSMT.git <path-to-your-local-folder>\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "A convenient method to experiment with this document consists in starting up a ``Jupyter Lab`` \\\ninstance: navigate to the ``docs/py_and_ipynb_examples`` folder of your local copy of the repository \\\nin a terminal session and execute the command ::\n\n     $ jupyter lab GCM2020.ipynb\n\nThis will open an interactive ``Jupyter`` session of the present notebook.\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Assuming that the current working directory of the Python instance is the aforementioned \\\n``docs/py_and_ipynb_examples`` sub-directory of the ``ReSMT`` package main directory, the package is \\\nloaded as follows:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "import os\nimport timeit\nimport sys\nsys.path.insert(0, os.path.abspath('../../'))\nimport resmt.datatypes as datatypes\nimport resmt.experiments as experiments\nimport resmt.visualizations as visualizations\n\nimport numpy\nimport networkx as nx\nimport z3"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "<div class=\"alert alert-info\"><h4>Note</h4><p>If running the Python file from within an IDE such as e.g. PyCharm, it is alternatively possible to change the working directory to the ``docs/py_and_ipynb_examples`` folder manually before running the above code within the IDE, as in ::\n\n    $ os.chdir('/local/path/to/ReSMT/docs/py_and_ipynb_examples')</p></div>\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The polymer model examples presented at GCM 2020\n------------------------------------------------\n\nConsider the following rewriting rules, for which only the \"plain\" rule parts are explicitly depicted, and \\\nwhich should be considered in the context of rewriting over **rigid graphs** (i.e. graphs respecting the \\\nconstraints of not containing any of the \"forbidden patterns\" mentioned in the previous examples):\n\n![](images/rGraphRules.svg)\n\n\nBased upon these rules, consider the following three overlap search examples, where we denote for a \\\ngiven linear rewriting rule $r=(O\\leftharpoonup I)$ by $out(r):=O$ and $in(r):=I$ \\\nthe out- and input interfaces of the rule $r$, respectively:\n\n- **Example P1:** $g_A = out(r_{create-edge})$, $g_B = in(r_{delete-edge})$\n- **Example P2:** $g_A = out(r_{c-c_2})$, $g_B = in(r_{b-c_2})$\n- **Example P3:** $g_A = out(r_{c-c_4})$, $g_B = in(r_{b-c_4})$\n- **Example P4:** $g_A = out(r_{c-c_7})$, $g_B = in(r_{b-c_7})$\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Running overlap search experiments with various alternative strategies\n----------------------------------------------------------------------\n\nIn each of the following experiments, all specification data necessary (i.e. the definition of the \\\ninput graphs, the forbidden patterns, output data file names and other parameters for the algorithms) \\\nare loaded from data files in the human-readable JSON format. Information on how these data files were \\\ngenerated via methods of the ``ReSMT`` package may be found in the **Appendix** section of this notebook.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# filenames for data files:\ngraphDataFname = 'data/GCM2020-graph-data'\nforbiddenPatternDataFname = 'data/GCM2020-forbidden-pattern-data'"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Experiment \\# 1:** a \"dry run\" with no constraints (in order to determine the number of overlaps \\\nwithout constraints as a reference)\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# specify the experiment specification data file:\nexpSpecDataFname = 'data/GCM2020-experiments-specification-no-constraints'\n\n# load the \"dry run\" experiment specification (for determining the number of overlaps without constraints):\nexpSpecData = []\nexpSpecData += [datatypes.loadExpsDataFromJSONfile(expSpecDataFname,\n                                                   'Experiments P1, P2, P3 and P4 without constraints')]\n\n# specify an output file for the result of the experiment:\nresultsDataFname = 'results/GCM2020-results-no-constraints'"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# run the experiments:\ndatatypes.runExperiments(expSpecData, resultsDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Experiment \\# 2:**  \"direct\"\" strategy computations\n\nIt is important to estimate the complexity of the problems for the larger examples **P3** and **P4**. \\\nFor example **P3**, the preparatory experiment of extracting the number of overlaps without constraints \\\nreveals that there are **2426** candidate overlaps to generate and test consider in this example. However, \\\nthe number of candidate overlaps for the example P4 is too large to even generate via the constraint-less \\\nalgorithm experiment. One may bound the number $n_{P4}$ of candidate overlaps from below by the \\\nnumber $n_{P4:V}$ of vertex-overlaps alone, and from above by the number $n_{P4:max}$ of \\\ninjective partial overlaps of the sets of elements (vertices and edges) of each graph, yet discarding the \\\nhomomorphism constraint. Utilizing the formula for the number of injective partial overlaps of two sets \\\n(see also the chapter **API experiments** in the examples gallery of the ``ReSMT`` documentation), \\\nwe thus find the following estimate:\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# load the graph data from the 'data/GCM2020-graph-data' JSON file:\nfName = 'data/GCM2020-graph-data'\ngraphData = datatypes.renderTDGdataFromJSONfile(fName)\n\n# number of vertices and edges in graph 'gA-P4':\nnumVgAP4 = len(graphData['gA-P4']['vDict'].keys())\nnumEgAP4 = len(graphData['gA-P4']['eDict'].keys())\n\n# number of vertices and edges in graph 'gB-P4':\nnumVgBP4 = len(graphData['gB-P4']['vDict'].keys())\nnumEgBP4 = len(graphData['gB-P4']['eDict'].keys())\n\n# lower bound for number of injective partial overlaps between the two graphs:\n# the number nP4V of vertex-only overlaps:\nnP4V = experiments.numSetInjectivePartialOverlaps(numVgAP4, numVgBP4)\n\n# upper bound for number of injective partial overlaps between the two graphs:\n# the number nP4max of overlaps of all graph elements, but discarding the\n# homomorphism constraints\nnP4max = experiments.numSetInjectivePartialOverlaps(numVgAP4 + numVgAP4, numVgBP4 + numVgBP4)\n\nprint('%s <= number of overlaps of gA-P4 and gB-P4 <= %s' % (nP4V, nP4max))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "\\begin{align}n_{P4:V} = 1174226049 \\quad \\leq \\quad n_{P4} \\quad \\leq \\quad n_{P4:max} = 30777323181433121931264\\end{align}\n\nIt is thus clear that the \"direct\" aka \"generate-and-test\" strategy is entirely infeasible for this \\\nparticular example, which is why example **P4** is not considered in the input data for this experiment.\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# specify the experiment specification data file:\nexpSpecDataFname = 'data/GCM2020-experiments-specification-direct-strategy'\n\n# load the \"direct\" strategy experiment specification:\nexpSpecData = []\nexpSpecData += [datatypes.loadExpsDataFromJSONfile(expSpecDataFname,\n                                                   'Experiments P1, P2, P3 and P4 with \\\"direct\\\" strategy')]\n\n# specify an output file for the result of the experiment:\nresultsDataFname = 'results/GCM2020-results-direct-strategy'"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# run the experiments:\ndatatypes.runExperiments(expSpecData, resultsDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Experiment \\# 3:**  \"DPE\" strategy computations\n\n<div class=\"alert alert-danger\"><h4>Warning</h4><p>Please be aware that the computations for the example P3 may take a _very_ long time on a \\\n     standard machine (on the order of several hours per individual run)!</p></div>\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# specify the experiment specification data file:\nexpSpecDataFname = 'data/GCM2020-experiments-specification-DPE-strategy'\n\n# load the \"DPE\" strategy experiment specification:\nexpSpecData = []\nexpSpecData += [datatypes.loadExpsDataFromJSONfile(expSpecDataFname,\n                                                   'Experiments P1, P2, P3 and P4 with \\\"DPE\\\" strategy')]\n\n# specify an output file for the result of the experiment:\nresultsDataFname = 'results/GCM2020-results-DPE-strategy'"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# run the experiments:\ndatatypes.runExperiments(expSpecData, resultsDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Experiment \\# 4:**  \"implicit\" strategy computations\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# specify the experiment specification data file:\nexpSpecDataFname = 'data/GCM2020-experiments-specification-implicit-strategy'\n\n# load the \"implicit\" strategy experiment specification:\nexpSpecData = []\nexpSpecData += [datatypes.loadExpsDataFromJSONfile(expSpecDataFname,\n                                                   'Experiments P1, P2, P3 and P4 with \\\"implicit\\\" strategy')]\n\n# specify an output file for the result of the experiment:\nresultsDataFname = 'results/GCM2020-results-implicit-strategy'"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# run the experiments:\ndatatypes.runExperiments(expSpecData, resultsDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Appendix: Generating the input data for the examples\n----------------------------------------------------\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Part 1: graph data**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# The file name for the graph data file:\nfName = 'data/GCM2020-graph-data'"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Example P1**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "S = z3.Solver()\n\nvDictA = {'vA1': '*', 'vA2': '*'}\neDictA = {'eA1': '-'}\nstDictA = {'eA1': ('vA1', 'vA2')}\ngA = datatypes.Z3TypedDirectedGraph(S, 'gA-P1', vDictA, eDictA, stDictA)\n\nvisA = visualizations.nxTypedDirectedGraph(gA,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gA-P1.svg',\n                                    gLayout='neato')\n\nvDictB = {'vB1': '*', 'vB2': '*'}\neDictB = {'eB1': '-'}\nstDictB = {'eB1': ('vB1', 'vB2')}\ngB = datatypes.Z3TypedDirectedGraph(S, 'gB-P1', vDictB, eDictB, stDictB)\n\nvisB = visualizations.nxTypedDirectedGraph(gB,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gB-P1.svg',\n                                    gLayout='neato')\n\n# write the two graphs to a JSON file, then read out the entire data and generate the two TDGS again\n# (to ensure both datasets are indeed stored properly):\ngA.writeTDGdataToJSONfile(fName)\ngB.writeTDGdataToJSONfile(fName)\n\n# display the graphs:\nvisualizations.displaySVGtable({'gA-P1': ['images/gA-P1.svg'], 'gB-P1': ['images/gB-P1.svg']}, width=\"100%\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Example P2**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "S = z3.Solver()\n\nvDictA = {'vA1': '*', 'vA2': '*', 'vA3': '*'}\neDictA = {'eA1': '-', 'eA2': '-', 'eA3': '-'}\nstDictA = {'eA1': ('vA1', 'vA2'), 'eA2': ('vA2', 'vA3'), 'eA3': ('vA3', 'vA1')}\ngA = datatypes.Z3TypedDirectedGraph(S, 'gA-P2', vDictA, eDictA, stDictA)\n\nvisA = visualizations.nxTypedDirectedGraph(gA,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gA-P2.svg',\n                                    gLayout='neato')\n\nvDictB = {'vB1': '*', 'vB2': '*', 'vB3': '*'}\neDictB = {'eB1': '-', 'eB2': '-'}\nstDictB = {'eB1': ('vB1', 'vB2'), 'eB2': ('vB2', 'vB3')}\ngB = datatypes.Z3TypedDirectedGraph(S, 'gB-P2', vDictB, eDictB, stDictB)\n\nvisB = visualizations.nxTypedDirectedGraph(gB,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gB-P2.svg',\n                                    gLayout='neato')\n\n\n# write the two graphs to a JSON file, then read out the entire data and generate the two TDGS again\n# (to ensure both datasets are indeed stored properly):\ngA.writeTDGdataToJSONfile(fName)\ngB.writeTDGdataToJSONfile(fName)\n\n# display the graphs:\nvisualizations.displaySVGtable({'gA-P2': ['images/gA-P2.svg'], 'gB-P2': ['images/gB-P2.svg']}, width=\"100%\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Example P3**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "S = z3.Solver()\n\nvDictA = {'vA1': '*', 'vA2': '*', 'vA3': '*', 'vA4': '*', 'vA5': '*'}\neDictA = {'eA1': '-', 'eA2': '-', 'eA3': '-', 'eA4': '-', 'eA5': '-'}\nstDictA = {'eA1': ('vA1', 'vA2'), 'eA2': ('vA2', 'vA3'), 'eA3': ('vA3', 'vA4'),\n           'eA4': ('vA4', 'vA5'), 'eA5': ('vA5', 'vA1')}\ngA = datatypes.Z3TypedDirectedGraph(S, 'gA-P3', vDictA, eDictA, stDictA)\n\nvisA = visualizations.nxTypedDirectedGraph(gA,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gA-P3.svg',\n                                    gLayout='neato')\n\nvDictB = {'vB1': '*', 'vB2': '*', 'vB3': '*', 'vB4': '*', 'vB5': '*'}\neDictB = {'eB1': '-', 'eB2': '-', 'eB3': '-', 'eB4': '-'}\nstDictB = {'eB1': ('vB1', 'vB2'), 'eB2': ('vB2', 'vB3'), 'eB3': ('vB3', 'vB4'), 'eB4': ('vB4', 'vB5')}\ngB = datatypes.Z3TypedDirectedGraph(S, 'gB-P3', vDictB, eDictB, stDictB)\n\nvisB = visualizations.nxTypedDirectedGraph(gB,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gB-P3.svg',\n                                    gLayout='neato')\n\n# write the two graphs to a JSON file, then read out the entire data and generate the two TDGS again\n# (to ensure both datasets are indeed stored properly):\ngA.writeTDGdataToJSONfile(fName)\ngB.writeTDGdataToJSONfile(fName)\n\n# display the graphs:\nvisualizations.displaySVGtable({'gA-P3': ['images/gA-P3.svg'], 'gB-P3': ['images/gB-P3.svg']}, width=\"100%\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Example P4**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "S = z3.Solver()\n\nvDictA = {'vA1': '*', 'vA2': '*', 'vA3': '*', 'vA4': '*',\n          'vA5': '*', 'vA6': '*', 'vA7': '*', 'vA8': '*'}\neDictA = {'eA1': '-', 'eA2': '-', 'eA3': '-', 'eA4': '-',\n          'eA5': '-', 'eA6': '-', 'eA7': '-', 'eA8': '-'}\nstDictA = {'eA1': ('vA1', 'vA2'), 'eA2': ('vA2', 'vA3'), 'eA3': ('vA3', 'vA4'), 'eA4': ('vA4', 'vA5'),\n           'eA5': ('vA5', 'vA6'), 'eA6': ('vA6', 'vA7'), 'eA7': ('vA7', 'vA8'), 'eA8': ('vA8', 'vA1')}\ngA = datatypes.Z3TypedDirectedGraph(S, 'gA-P4', vDictA, eDictA, stDictA)\n\nvisA = visualizations.nxTypedDirectedGraph(gA,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gA-P4.svg',\n                                    gLayout='neato')\n\nvDictB = {'vB1': '*', 'vB2': '*', 'vB3': '*', 'vB4': '*',\n          'vB5': '*', 'vB6': '*', 'vB7': '*', 'vB8': '*'}\neDictB = {'eB1': '-', 'eB2': '-', 'eB3': '-', 'eB4': '-',\n          'eB5': '-', 'eB6': '-', 'eB7': '-'}\nstDictB = {'eB1': ('vB1', 'vB2'), 'eB2': ('vB2', 'vB3'), 'eB3': ('vB3', 'vB4'), 'eB4': ('vB4', 'vB5'),\n           'eB5': ('vB5', 'vB6'), 'eB6': ('vB6', 'vB7'), 'eB7': ('vB7', 'vB8')}\ngB = datatypes.Z3TypedDirectedGraph(S, 'gB-P4', vDictB, eDictB, stDictB)\n\nvisB = visualizations.nxTypedDirectedGraph(gB,\n                                    drawGraph=True,\n                                    writeGraph=True,\n                                    fileName='images/gB-P4.svg',\n                                    gLayout='neato')\n\n# write the two graphs to a JSON file, then read out the entire data and generate the two TDGS again\n# (to ensure both datasets are indeed stored properly):\ngA.writeTDGdataToJSONfile(fName)\ngB.writeTDGdataToJSONfile(fName)\n\n# display the graphs:\nvisualizations.displaySVGtable({'gA-P4': ['images/gA-P4.svg'], 'gB-P4': ['images/gB-P4.svg']}, width=\"100%\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Part 2: forbidden pattern data**\n\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# The file name for the forbidden pattern data file:\nfName = 'data/GCM2020-forbidden-pattern-data'"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Definition of the four \"forbidden\" TDG patterns:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "Saux = z3.Solver()\n\nvDictN1 = {'v'+str(i): '*' for i in range(3)}\neDictN1 = {'e'+str(i): '-' for i in range(1, 3)}\nstDictN1 = {'e'+str(i): ('v0', 'v'+str(i)) for i in range(1, 3)}\ngraphN1 = datatypes.Z3TypedDirectedGraph(Saux, 'gN1', vDictN1, eDictN1, stDictN1)\n\nvisN1 = visualizations.nxTypedDirectedGraph(graphN1,\n                                            drawGraph=True,\n                                            writeGraph=True,\n                                            fileName='images/graphN1.svg',\n                                            gLayout='neato')\n\nvDictN2 = {'v'+str(i): '*' for i in range(3)}\neDictN2 = {'e'+str(i): '-' for i in range(1, 3)}\nstDictN2 = {'e'+str(i): ('v'+str(i), 'v0') for i in range(1, 3)}\ngraphN2 = datatypes.Z3TypedDirectedGraph(Saux, 'gN2', vDictN2, eDictN2, stDictN2)\n\nvisN2 = visualizations.nxTypedDirectedGraph(graphN2,\n                                            drawGraph=True,\n                                            writeGraph=True,\n                                            fileName='images/graphN2.svg',\n                                            gLayout='neato')\n\nvDictN3 = {'v1': '*', 'v2': '*'}\neDictN3 = {'e1': '-', 'e2': '-'}\nstDictN3 = {'e1': ('v1', 'v2'), 'e2': ('v1', 'v2')}\ngraphN3 = datatypes.Z3TypedDirectedGraph(Saux, 'gN3', vDictN3, eDictN3, stDictN3)\n\nvisN3 = visualizations.nxTypedDirectedGraph(graphN3,\n                                            drawGraph=True,\n                                            writeGraph=True,\n                                            fileName='images/graphN3.svg',\n                                            gLayout='neato')\n\nvDictN4 = {'v1': '*'}\neDictN4 = {'e1': '-', 'e2': '-'}\nstDictN4 = {'e1': ('v1', 'v1'), 'e2': ('v1', 'v1')}\ngraphN4 = datatypes.Z3TypedDirectedGraph(Saux, 'gN4', vDictN4, eDictN4, stDictN4)\n\nvisN4 = visualizations.nxTypedDirectedGraph(graphN4,\n                                            drawGraph=True,\n                                            writeGraph=True,\n                                            fileName='images/graphN4.svg',\n                                            gLayout='neato')\n\n\n# write the forbidden graph patterns to a JSON file, then read out the entire data\n# and generate the two TDGS again (to ensure both datasets are indeed stored properly):\nfor gN in [graphN1, graphN2, graphN3, graphN4]:\n    gN.writeTDGdataToJSONfile(fName)\n\n# display the forbidden graph patterns:\nvisualizations.displaySVGtable({\n    'gN1': ['images/graphN1.svg'],\n    'gN2': ['images/graphN2.svg'],\n    'gN3': ['images/graphN3.svg'],\n    'gN4': ['images/graphN4.svg'],\n}, width=\"100%\")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "# render the forbidden relation data:\nfpData = datatypes.renderTDGdataFromJSONfile(fName)\n\nSaux = z3.Solver()\n\nfpList = [datatypes.Z3TypedDirectedGraph(Saux, x['gName'], x['vDict'], x['eDict'], x['stDict']) for key, x in fpData.items()]\n\nfrClist = []\n\nfor gN in fpList:\n    if fpData[gN.name].get('frData', {}) == {}:\n        # if the forbidden relation data is not yet present in fpData, generate it:\n        frC = datatypes.Z3TDGfrContainer(Saux, gN)\n        frC.genTDGfrData()\n        frClist += [frC]\n\n        # store the forbidden relation data to the fpData (e.g. for use in the next runs):\n        fpData[gN.name]['frData'] = frC.frData\n\n    else:\n        # if the forbidden relation data is present, use it to instantiate the frContainer:\n        frClist += [datatypes.Z3TDGfrContainer(Saux, gN, fpData[gN.name]['frData'])]\n\n# for buffering the computationally expensive forbidden relation computations, save all the fpData\n# to the JSON file with name expSpec.forbiddenPatternDataFname:\n\nwith open(fName + '.json', 'w') as f:\n    json.dump(fpData, f, sort_keys=True, indent=2)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**Part 3: Specification data for the experiments**\n\n\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Input graph and forbidden pattern JSON file names:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "graphDataFname = 'data/GCM2020-graph-data'\nforbiddenPatternDataFname = 'data/GCM2020-forbidden-pattern-data'"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The \"dry runs\" for determining the number of overlaps without constraints:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "experimentName = 'Experiments P1, P2 and P3 without constraints'\nexperimentsData = [('gA-P1', 'gB-P1'), ('gA-P2', 'gB-P2'), ('gA-P3', 'gB-P3')]\nstrategyName = 'noConstraints'\nexpSpecDataFname = 'data/GCM2020-experiments-specification-no-constraints'\n\nnumberOfRepetitions = 1\nmaxRunTime = 0    # <- '0' is the key value to parse as no limitation on the run time!\n\nexpsDataNoConstraints = datatypes.ExperimentSpec(experimentName,\n                                     graphDataFname,\n                                     forbiddenPatternDataFname,\n                                     strategyName,\n                                     numberOfRepetitions,\n                                     experimentsData,\n                                     maxRunTime)\n\nexpsDataNoConstraints.storeExpsDataToJSONfile(expSpecDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The \"direct\" strategy runs:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "experimentName = 'Experiments P1, P2 and P3 with \\\"direct\\\" strategy'\nexperimentsData = [('gA-P1', 'gB-P1'), ('gA-P2', 'gB-P2'), ('gA-P3', 'gB-P3')]\nstrategyName = 'direct'\nexpSpecDataFname = 'data/GCM2020-experiments-specification-direct-strategy'\n\nnumberOfRepetitions = 5\nmaxRunTime = 0    # <- no limit on maximal runtime\n\nexpsDataDirectStrategy = datatypes.ExperimentSpec(experimentName,\n                                                  graphDataFname,\n                                                  forbiddenPatternDataFname,\n                                                  strategyName,\n                                                  numberOfRepetitions,\n                                                  experimentsData,\n                                                  maxRunTime)\n\nexpsDataDirectStrategy.storeExpsDataToJSONfile(expSpecDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The \"DPE\" strategy runs :\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "experimentName = 'Experiments P1, P2, P3 and P4 with \\\"DPE\\\" strategy'\nexperimentsData = [('gA-P1', 'gB-P1'), ('gA-P2', 'gB-P2'), ('gA-P3', 'gB-P3'), ('gA-P4', 'gB-P4')]\nstrategyName = 'DPE'\nexpSpecDataFname = 'data/GCM2020-experiments-specification-DPE-strategy'\n\nnumberOfRepetitions = 5\nmaxRunTime = 0    # <- no limit on maximal runtime\n\nexpsDataDPEStrategy = datatypes.ExperimentSpec(experimentName, graphDataFname,\n                                               forbiddenPatternDataFname,\n                                               strategyName,\n                                               numberOfRepetitions,\n                                               experimentsData,\n                                               maxRunTime)\n\nexpsDataDPEStrategy.storeExpsDataToJSONfile(expSpecDataFname)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The \"implicit\" strategy runs:\n\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "collapsed": false
      },
      "outputs": [],
      "source": [
        "experimentName = 'Experiments P1, P2, P3 and P4 with \\\"implicit\\\" strategy'\nexperimentsData = [('gA-P1', 'gB-P1'), ('gA-P2', 'gB-P2'), ('gA-P3', 'gB-P3'), ('gA-P4', 'gB-P4')]\nstrategyName = 'implicit'\nexpSpecDataFname = 'data/GCM2020-experiments-specification-implicit-strategy'\n\n\nnumberOfRepetitions = 5\nmaxRunTime = 0    # <- no limit on maximal runtime\n\nexpsDataImplicitStrategy = datatypes.ExperimentSpec(experimentName,\n                                                  graphDataFname,\n                                                  forbiddenPatternDataFname,\n                                                  strategyName,\n                                                  numberOfRepetitions,\n                                                  experimentsData,\n                                                  maxRunTime)\n\nexpsDataImplicitStrategy.storeExpsDataToJSONfile(expSpecDataFname)"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.2"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}