# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html


# -- Import statements -------------------------------------------------------
from datetime import date

import os
import sys

# patch for wrong utf8 encoding behavior
# SOURCE: https://stackoverflow.com/questions/57259459/generate-sphinx-autodoc-for-files-that-contain-encoded-byte-strings

from autoapi.mappers.python.parser import Parser

def patched_encode(self, to_encode):
    if self._encoding:
        try:
            if not(isinstance(bytes, to_encode)):  # <- The patch
                return _TEXT_TYPE(to_encode, self._encoding)
        except TypeError:
            # The string was already in the correct format
            pass

    return to_encode

Parser._encode = patched_encode

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------

project = u'ReSMT'
td = date.today()
author = 'Nicolas Behr'
copyright = u'2020, Nicolas Behr. Last updated on %s.' % (td.isoformat())

# The full version, including alpha/beta/rc tags
version = '0.0.3'
release = '0.0.3-gitlab'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    # 'sphinx.ext.autosummary',
    'sphinx_copybutton',  # for "copy to clipboard" buttons
    'sphinxcontrib.bibtex',  # for bibliographic references
    'sphinxcontrib.rsvgconverter',  # for SVG->PDF conversion in LaTeX output
    'sphinx_gallery.gen_gallery',
    'sphinx_math_dollar',  # to fix a problem with LaTeX in gallery examples
    'sphinx_gallery.load_style',  # load CSS for gallery (needs SG >= 0.6)
    # 'sphinx_last_updated_by_git',  # get "last updated" from Git
    'sphinx.ext.graphviz',
    'sphinx.ext.imgmath',
    'sphinx.ext.coverage',
    'sphinx.ext.napoleon',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
    'sphinx_pyreverse'
]

# autodoc/autosummary configuration:
# autosummary_generate = True
autodoc_default_options = {'inherited-members': None}

# The master toctree document.
master_doc = 'index'

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'friendly'

# include todo notes:
todo_include_todos = False


# imgmath settings:
imgmath_image_format = 'svg'

# Variant for Bootstrap theme, i.e. light grey color for math on dark background:
# imgmath_latex_preamble='\\usepackage{newtxsf}\\usepackage{amsmath}\\usepackage{mathtools}\\usepackage{xcolor}\\definecolor{mathColor}{RGB}{196,196,196}\\usepackage{tcolorbox}\\usepackage{everysel}\\everymath{\\color{mathColor}}'

# variant for the ReadTheDocs theme:
imgmath_latex_preamble='\\usepackage{newtxsf}\\usepackage{amsmath}\\usepackage{mathtools}\\usepackage{xcolor}\\usepackage{tcolorbox}'

numfig = True
math_numfig = True
numfig_secnum_depth = 2
math_eqref_format = "Eq.{number}"

# The name of an image file (relative to this directory) to place at the top of
# the title page.
latex_logo = 'ReSMT-logo.pdf'

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
latex_use_parts = True

# Napoleon settings

# setting to choose the type of docstrings for the napoleon extension:
napoleon_google_docstring = True
napoleon_numpy_docstring = False
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

### ReadTheDocs theme options:

html_theme = "sphinx_rtd_theme"

html_theme_options = {
    'canonical_url': '',
   # 'analytics_id': 'UA-XXXXXXX-1',  #  Provided by Google in your dashboard
    'logo_only': False,
    'display_version': True,
    'prev_next_buttons_location': 'None',
    'style_external_links': True,
    # 'vcs_pageview_mode': '',
    #'style_nav_header_background': 'white',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 5,
    'includehidden': True,
    'titles_only': False
}

html_logo = 'ReSMT-logo.svg'

# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False

# Output file base name for HTML help builder.
htmlhelp_basename = 'ReSMT-doc'

# These folders are copied to the documentation's HTML output
html_static_path = ['_static']

# adjusting the RTD theme's interaction with sphinx-gallery (i.e. removing all download links):
html_css_files = [
    'css/custom.css',
]


# LaTeX rendering setup:
latex_engine = 'xelatex'
latex_elements = {#
#    'fontpkg': r'''
#\setmainfont{DejaVu Serif}
#\setsansfont{DejaVu Sans}
#\setmonofont{DejaVu Sans Mono}
#''',
    'papersize': 'a4paper',
    'pointsize': '10pt',
    'sphinxsetup': 'verbatimwithframe=False',
    'preamble': r'''
        \usepackage{amsmath}
        \usepackage{mathtools}
        \usepackage{xcolor}
        \usepackage{charter}
        \usepackage[defaultsans]{lato}
        \usepackage{inconsolata}
    '''#,
#    'fncychap': r'\usepackage[Bjornstrup]{fncychap}',
#    'printindex': r'\footnotesize\raggedright\printindex',
}


# latex_show_urls = 'footnote'


### sphinx-gallery configuration


sphinx_gallery_conf = {
    'examples_dirs': 'examples',   # path to your example scripts
    'gallery_dirs': 'py_and_ipynb_examples',  # path to where to save gallery generated output
    # directory where function/class granular galleries are stored
    'backreferences_dir': 'gen_modules/backreferences',
    # 'image_scrapers': (SVGScraper(),),
    'capture_repr': (),
    # set the parameter below to a high value to suppress timing reports
    # (which are meaningless since we do not run the examples directly at
    # the time of rendering the documentation)
    'min_reported_time': 100000000,
    # switch off memory usage reports
    'show_memory': False,
    'thumbnail_size': (250, 250),
    # Patter to search for example files
    # "filename_pattern": r"\.py",
    # Remove the "Download all examples" button from the top level gallery
    "download_all_examples": False,
    'download_section_examples': False,
    # 'line_numbers': True,
    # 'plot_gallery': 'False',
    'first_notebook_cell': ("%matplotlib inline\n"
                            "from IPython.display import SVG, display, Image")
}

numfig = False

import warnings

# Remove matplotlib agg warnings from generated doc when using plt.show
warnings.filterwarnings("ignore", category=UserWarning,
                        message='Matplotlib is currently using agg, which is a'
                                ' non-GUI backend, so cannot show the figure.')
