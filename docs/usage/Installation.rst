.. _installation_instructions:

Installation instructions
=========================

.. note:: The Python version utilized for the development of this package has been ``3.8.2``. It is thus \
    recommended to install this version via ``pyenv`` (especially on ``macOs``), but likely versions of \
    Python ``>3.8.2`` will be equally compatible.

.. warning:: While in principle an equivalent ``Anaconda Python`` distribution with ``Python 3`` could be \
    used, unfortunately some of the core packages required (in particular the Z3 solver API package) are only \
    available via the ``conda`` installation procedures in outdated form, which is why it is **not** \
    recommended to try running this package via this type of Python distribution.

Assuming a valid Python 3 environment has been set up, please follow the steps below in order to obtain a \
working environment for experimenting with the ``ReSMT`` package:

1. Clone or download the GitLab repository of ``ReSMT``.
2. Navigate to the local copy of the repository, and execute the following command: ::

    $ python3 -mpip install -U -r requirements.txt

Installation of requisite command-line tools
--------------------------------------------

The package relies upon the ``poppler`` and ``pdf2svg`` command-line tools for some of the PDF and SVG rendering tasks. Please install these tools in a recent version e.g. via ``apt-get`` on Linux systems, or via the following command on ``macOs``: ::

    $ brew install poppler pdf2svg

Setup of ``JupyterLab`` Jupyter notebook client (optional)
----------------------------------------------------------
In order to experiment with the Jupyter notebooks containing \
examples, a convenient option is the following one (https://github.com/jupyterlab/jupyterlab): ::

    $ python3 -m pip install -U jupyterlab
    $ python3 -m pip install -U ipykernel  # install the python kernel (ipykernel) globally
    $ python3 -m ipykernel install      # install python kernel into nteract's available kernel list


Additional information for macOS Catalina
-----------------------------------------

 .. warning:: Due to the internal architecture of `macOS Catalina`, it is in general **not** advisable \
    to directly install `Python 3` as would be the case on a `Linux OS`. While there would in principle \
    be an option available to install `Python 3` via `Homebrew`, the instructions below follow the current \
    "best practices" advice (as described e.g. `here`_), whereby it is advantageous to utilize ``pyenv`` \
    in order to install (possibly various alternative versions of) `Python 3` *without* touching the \
    native `Python 2.7` installation of `macOS`.

 .. _here: https://opensource.com/article/19/5/python-3-default-mac


Installing a `Python 3` version via ``pyenv``
*********************************************
First, we need a working ``pyenv`` installation, e.g. via invoking Homebrew, and assuming that the shell \
interface is the standard ZSH one (see the `pyenv documentation`_ for alternative configurations): ::

    $ brew install pyenv
    $ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
    $ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc

Afterwards, execute the following to install the latest ``Python 3`` version (for the guide and during the \
development of this package, this version has been ``Python 3.8.2``): ::

    $ pyenv install 3.8.2
    $ pyenv global 3.8.2      # activates Python 3.8.2 as the global Python version
    $ pyenv versions          # check which versions are installed, with active one marked "*"
            system
           *3.8.2               # -> Python 3.8.2 is the currently active version!

The advantage of this method is that ``pyenv`` on the one hand installs all Python versions in \
virtual environments, but then in addition emulates to the OS that the activated version is a \
locally installed one.

.. _pyenv documentation:
    https://github.com/pyenv/pyenv
