Documentation of design choices vs. original MGS codes
------------------------------------------------------

Choice of Python platform
*************************

We opted for ``Python 3`` instead of ``Python 2`` in order to allow for
    - type annotations
    - parameter and alias features
    - future-proofing
    
Code aesthetics
***************

* We prefer to issue an ``import z3`` statement as per various style guides, s.th. in the source code \
  it is clear which function is native tot he Z3 API and which one is not.

* We documented each of the core functions extensively using reSTructured Text and following the \
  Google Python Docstring guidelines.



.. note :: There is a subtle point in Python 3 regarding how to annotate the output type of the ``__init__``  \
    which has to be annotated as ``None`` for technical reasons according to `PEP 484`_:

        Note that the return type of ``__init__`` ought to be annotated with ``-> None``.
            The reason for this is subtle. If ``__init__`` assumed a return annotation of ``-> None``, \
            would that mean that an argument-less, un-annotated ``__init__`` method should still be \
            type-checked? Rather than leaving this ambiguous or introducing an exception to the exception, \
            we simply say that ``__init__`` ought to have a return annotation; the default behavior is \
            thus the same as for other methods.

.. _PEP 484:
    https://www.python.org/dev/peps/pep-0484/