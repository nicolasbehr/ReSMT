API reference
=============


.. automodule:: resmt.datatypes
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: resmt.experiments
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: resmt.visualizations
   :members:
   :undoc-members:
   :show-inheritance:

.. include:: notesZ3APItypes.rst

Gallery of Sourcetrail class diagrams
-------------------------------------

The diagrams in this gallery have been generated via the \
`Sourcetrail <https://www.sourcetrail.com>`_ open-source cross-platform source-code explorer.



The class diagram for the current development version of the `ReSMT` package
****************************************************************************

.. figure:: ../_static/sourcetrailFigs/resmt-overview.*
   :alt: resmt-overview.svg

   Illustration of the currently implemented ``Z3TypedSet`` and ``Z3TypedDirectedGraph`` classes and \
   related structures (right \
   click and choose ``Open Image In New Tab`` for full-scale view).


The class diagram of the official `Z3 Python API`
*************************************************

.. figure:: ../_static/sourcetrailFigs/Z3API-Solver.*
   :alt: Z3API-Solver.svg

   The analysis of the full `Z3 Python API package` Version `4.8.7` (right \
   click and choose ``Open Image In New Tab`` for full-scale view).


