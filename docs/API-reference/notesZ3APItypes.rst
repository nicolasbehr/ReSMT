A note on the Python types of the native Z3 API
-----------------------------------------------

Executing the following code permits to determine the types of the different kinds of \
instances accessible via the official Z3 Python API: ::

    import z3

    A = z3.DeclareSort('A')     # a Z3 sort declaration
    X = z3.Const('X', A)        # a Z3 constant expression declaration
    F = z3.Function('F', A, A)  # a Z3 function expression declaration

    # a Z3 assertion declarations

    ast1 = z3.ForAll(z3.Const('Y', A), z3.Const('Y', A) == X)
    ast2 = z3.Exists(z3.Const('Z',A), F(z3.Const('Z',A)) == X)

    ast12 = z3.And([ast1, ast2])

    s = z3.Solver()
    s.add(ast12)
    sC = s.check()
    sM = s.model()

    for z in [A,X,F, F(X), ast1, ast2, ast12, s, sC, sM]:
        print(type(z))

The results of this experiment are as follows:

.. list-table:: Python types of instances generated with the Z3 API
   :header-rows: 1

   * - Expression ``x``
     - ``type(x)``
   * - ``A = z3.DeclareSort('A')``
     - ``z3.SortRef``
   * - ``X = z3.Const('X', A)``
     - ``z3.ExprRef``
   * - ``F = z3.Function('F', A, A)``
     - ``z3.FuncDeclRef``
   * - ``F(X)``
     - ``z3.ExprRef``
   * - ``ast1 = z3.ForAll(z3.Const('Y', A), z3.Const('Y', A) == X)``
     - ``z3.QuantifierRef``
   * - ``ast2 = z3.Exists(z3.Const('Z',A), F(z3.Const('Z',A)) == X)``
     - ``z3.QuantifierRef``
   * - ``ast12 = z3.And([ast1, ast2])``
     - ``z3.BoolRef``
   * - ``s = z3.Solver()``
     - ``z3.Solver``
   * - ``sC = s.check()``
     - ``z3.CheckSatResult``
   * - ``sM = s.model()``
     - ``z3.ModelRef``
