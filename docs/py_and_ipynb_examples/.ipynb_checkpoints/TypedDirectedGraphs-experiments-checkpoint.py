"""
Typed directed graphs
=====================

"""

#%%
# Preliminaries: setting up the Jupyter environment
# -------------------------------------------------
#
# A convenient method to experiment with this document consists in starting up a ``Jupyter Lab`` instance: navigate
# to the ``py_and_ipynb_examples`` folder of your local copy of the repository in a terminal and execute the command ::
#
#   $ jupyter lab TypedDirectedGraphs-experiments.ipynb
#
# This will open an interactive ``Jupyter`` session of the present notebook.
#


#%%
# Assuming that the current working directory of the Python instance is the ``docs/examples/`` sub-directory of the ``ReSMT``  package main directory, the package is loaded as follows:


import os
import sys
sys.path.insert(0, os.path.abspath('../../resmt'))
import datatypes

import numpy
import networkx as nx
import z3


#%%
# .. note:: If running the Python file from within an IDE such as e.g. PyCharm, it is alternatively possible to change the working directory to the ``docs/py_and_ipynb_examples`` folder manually before running the above code within the IDE, as in ::
#
#     $ os.chdir('/local/path/to/ReSMT/docs/py_and_ipynb_examples')


#%%
# Overview of datatype design principles and first examples
# ---------------------------------------------------------
#
# The design philosophy of ``ReSMT`` consists in attempting a direct *translation* of the mathematical \
# concepts developed in categorical rewriting theories into first order logic formulas amenable to \
# be further analysed via an SMT solver or theorem prover. As such, every datatype in this library \
# serves the dual purpose of faithfully encoding the category-theoretical datatypes as well as the \
# encoding of the data into logical formulas. We exemplify this concept with two of the base datatypes. \
# i.e. the classes ``Z3TypedSet`` of typed sets and ``Z3TypedFunction`` for functions of typed sets. \
# Let us first set up some instances of these classes, highlighting in particular the special cases of \
# empty sets and functions thereof. Note that all constructions require a ``Z3`` solver instance, \
# which may be set up as follows:

S = z3.Solver()

#%%
# Typed sets
# ----------
#

emptySet = datatypes.Z3TypedSet(S, 'emptySet', {})

#%%
# It is important to note that an empty set for technical reasons still must contain a \
# *nil-element*, and that (since all sets for this module must be typed) this nil-element \
# is assigned a default type ``'_DEFAULT'``:

print(emptySet.els)             # returns {}, as the set has no elements
print(emptySet.sorts)           # the only sort is '_DEFAULT'
print(emptySet.nilEls)          # the sort '_DEFAULT' is inhabited by only the nil-element of that sort

#%%
# We next consider an example of a non-empty typed set:


setA = datatypes.Z3TypedSet(S, 'setA', {'a':'elSort1', 'b':'elSort1', 'c':'elSort2'})

#%%
# The following examples illustrate our ``datatypes`` API, demonstrating how the dictionary of \
# elements with names as keys and sorts as values, the set of  element sorts, the dictionary of \
# nil-elements accessed by sorts as keys, and finally the direct access to elements by their names \
# are accessible:

print(setA.els)             # element name:sort dictionary
print(setA.sorts)           # set of sorts in the set
print(setA.getEl('a'))      # retrieves the Z3 constant assigned to the element

#%%
# A more subtle point of our implementation concerns the concrete encoding of the finite typed sets \
# in terms of the ``Z3`` API. Technically, a finite set is encoded as a ``z3.EnumSort``, which entails \
# in practice that for each subset of elements of a given sort, one such enumeration sort is \
# instantiated, and for each of the elements plus for the nil-element of the sort, a special \
# ``Z3`` constant is instantiated as part of the ``z3.EnumSort`` instantiation. In effect, the \
# ``Z3`` enumeration sort permits to instantiate a ``Z3`` constant (of that sort) that faithfully \
# encodes the concept of "an element in the finite subset of the given sort", which is a key concept \
# in many of the algorithms of ``ReSMT``. For this reason, this important notion of "one enumeration \
# sort per element sort" is made accessible via the ``.subsets[srt]`` field of a ``Z3TypedSet`` instance, \
# which stores the aforementioned instantiation of the enumeration sort, while the field \
# ``.subsetEls[srt]`` stores all of the ``Z3`` constants representing the elements of the finite \
# subset of sort ``srt``:

print(setA.subsets)                     # renders a dictionary with all enumeration sorts (by sort name)
print(setA.subsetEls['elSort1'])        # the Z3 constants [nil|elSort1, a, b] "inhabiting" the first sort
print(setA.subsetEls['elSort2'])        # the Z3 constants [nil|elSort2, c] "inhabiting" the second sort


#%%
# Functions of typed sets
# -----------------------
#
# Given two typed sets ``setA`` and ``setB``, a function between these two typed sets is effectively \
# a collection of functions between the subsets induced by sorts  of elements. For technical reasons, \
# we currently only support **one codomain sort per domain sort**, i.e. we do not allow **sort-signatures** \
# where elements of a given domain sort are mapped by the functions to codomain elements of more \
# than one sort. Another fine-detail concerns the **totality** of functions: if a non-total function \
# is specified, this is typically used to set up a "template" of a typed function, such as in cases \
# where a given algorithm should construct a function via the ``Z3`` methods. The nature of the \
# function being initialized as non-total is also documented in the log file for the module. Let us \
# first consider an example of a total typed function:

setD = datatypes.Z3TypedSet(S, 'domSet', {'a1': 'sortX', 'a2': 'sortX', 'a3': 'sortY'})
setC = datatypes.Z3TypedSet(S, 'codomSet', {'b1': 'sortX', 'b2': 'sortX', 'b3': 'sortY', 'b4': 'sortZ'})
f = datatypes.Z3TypedFunction(S,'f', setD, setC, {'a1': 'b2', 'a2': 'b1', 'a3': 'b4'}, isInjective = True)

#%%
# The sort maps of ``f`` are accessible via the field ``f.srtMaps``:
print(f.srtMaps)

#%%
# Subfunctions are stored in the field ``.subFunctions`` as a dictionary with keys the sort names \
# and values the ``Z3`` function instances for the subfunctions:
print(f.subFunctions)

#%%
# Most importantly, the instantiation of a ``Z3TypedFunction`` has the side-effect of uploading \
# logical assertions to the chosen ``Z3`` solver instance that encode the properties of the typed \
# function, such as in particular the consistent sort-wise mapping of nil-elements, the injectivity \
# or surjectivity (if specified) and any concrete mapping that was specified in the ``fData`` part \
# of the input data. This information may also be accessed directly via the ``.asts`` field:
for ast in f.asts:
    print(ast)


#%%
# As a special case, consider below as another example a function of empty sets:

emptySet = datatypes.Z3TypedSet(S, 'emptySet', {})
g = datatypes.Z3TypedFunction(S,'g', emptySet, emptySet, {})

#%%
# One may verify that indeed the carrier sets are just the nil-elements of ``'_DEFAULT'`` sort, and \
# that the assertions made are purely the nil-element mappings:

print(g.domSet.els)
print(g.domSet.nilEls)
print(g.codomSet.els)
print(g.codomSet.nilEls)
for ast in g.asts:
    print(ast)

#%%
# Predicates over typed sets
# --------------------------
#
# For several of our algorithms, we need to define predicates over typed sets. The first kind of \
# predicate is a simple Boolean predicate, which internally is implemented as a collection of \
# Boolean predicates (one for each subset induced by element sorts). Note that this type of object \
# inherits the ``Z3`` solver instance from its domain set upon instantiation! Note also that \
# strictly speaking the instantiation given below does not fully specify a Boolean predicate beyond \
# some elementary consistency assertions (asserting that the nil-elements are always carrying the \
# predicate ``True``), since in the use cases present in ``ReSMT`` the predicates will be further \
# refinedd as part of certain ``Z3``-based search algorithms. As a somewhat counter-intuitive \
# consequence, an instance of such a predicate is not "functional" per se, in that only upon \
# rendering a model for the assertions that encode the predicate can one evaluate it in the form of \
# this model on concrete elements.

setD = datatypes.Z3TypedSet(S, 'domSet', {'a1': 'sortX', 'a2': 'sortX', 'a3': 'sortY'})
predD = datatypes.Z3TypedSetPredicate('predD', setD)

for ast in predD.asts:
    print(ast)

#%%
# The second type of predicate implemented in ``ReSMT`` is that of a **typed span predicates**, or, \
# more precisely, templates thereof. The idea of this construction is that these predicates encode \
# **partial overlaps** of typed sets, and that the ternary predicate functions encode which elements \
# are in "partial overlap" via the span.

setA = datatypes.Z3TypedSet(S, 'setA', {'a1':'sortX', 'a2':'sortX', 'a3': 'sortY'})
setB = datatypes.Z3TypedSet(S, 'setB', {'b1':'sortX', 'b2':'sortX', 'b3': 'sortY', 'b4':'sortZ'})

#%%
# Note that one must ensure that both sets have the exact same sort content, which may be \
# achieved via "patching" suitable nil-elements if necessary as follows:

setA.patchNilElements(setB)
setB.patchNilElements(setA)

spanPredAB = datatypes.Z3TypedSetSpanPredicateTemplate('predAB', setA, setB, isMonic=True)

#%%
# Postponing an in-detail discussion of this concept to a later point (i.e. when discussing the \
# TDG overlap finding routines), suffice it here to illustrate the assertions instantiated for a \
# typed set span predicate template, which encode consistent nil-element mapping as well as \
# "bi-injectivity" in case of monic span predicates:

for ast in spanPredAB.asts:
    print(ast)

#%%
# Finally, we will need a form of template for automorphisms of typed sets, which again will serve \
# in later algorithms to search for such automorphisms via ``Z3`` (rather than specifying such \
# automorphisms manually).

setD = datatypes.Z3TypedSet(S, 'domSet', {'a1': 'sortX', 'a2': 'sortX', 'a3': 'sortY'})  # the domain set
autD = datatypes.Z3TypedSetAutomorphismTemplate('autD', setD)        # the automorphism template

print('\n Assertions encoding the automorphism property:\n')
for ast in autD.asts:
    print(asts)


#%%
# Introducing typed directed graphs
# ---------------------------------
#
# A typed directed graph (TDG) is the following collection of data:
#
# 1. A set of vertices :math:`V` and a set of edges :math:`E`.
# 2. Source and target functions :math:`s,t: E\rightarrow V`.
# 3. Typing functions :math:`\tau_V:V\rightarrow T_V` and :math:`\tau_E:E\rightarrow T_E`.
#
# **Note:** For technical reasons, at the moment we have not yet implemented the most general possible \
# form of typing as described above, but only a variant where a given edge type is equipped with a **fixed** \
# type-signature, i.e. all edges of a given type have a particular source- and target-type. This feature may be \
# changed in a future version of the code. Concretely, this entails the following statement:
#
# .. math::
#   \forall e\in E: \tau_E(e) &= t_e\,,\; \tau_V(s(e))= t_{s(e)}\,. \; \tau_V(t(e)) = t_{t(e)}\\
#   \Rightarrow \quad \forall e'\in E:\tau_E(e')&=t_e: \tau_V(s(e'))=t_{s(e)} \land \tau_V(t(e'))=t_{t(e)}\,.
#
#
# Let us first experiment with the API provided in the ``datatypes.py`` in order to set up a \
# typed directed graph, en passent also illustrating the data format for entering TDGs. \
# We first demonstrate how to set up an empty TDG.

Gempty = datatypes.Z3TypedDirectedGraph(S, 'gEmpty', {}, {}, {})

#%%
# As expected, this graph only contains nil-elements of the ``'_DEFAULT'`` sort for the vertex \
# and for the edge carrier sets, and just an assertion that the nil-edge has the nil-vertex as \
# both its source and target:

print(Gempty.vertices.els)
print(Gempty.vertices.nilEls)
print(Gempty.edges.els)
print(Gempty.edges.nilEls)

print(Gempty.src.asts)
print(Gempty.trgt.asts)

#%%
# Next, let us instantiate a non-empty typed directed graph:

vDictA = {'v1':'sortX', 'v2': 'sortX', 'v3': 'sortY'}                       # the vertex carrier set
eDictA = {'e1':'sortW', 'e2': 'sortT'}                                      # the edge carrier set
stDictA = {'e1':('v1','v2'), 'e2':('v3','v1')}                              # source/target incidence data
graphA = datatypes.Z3TypedDirectedGraph(S, 'gA', vDictA, eDictA, stDictA)

#%%
# Here is an overview of how this data is encoded:
print('\n Vertex carrier set:\n')
print(graphA.vertices.els)
print(graphA.vertices.nilEls)
print('\n Edge carrier set:\n')
print(graphA.edges.els)
print(graphA.edges.nilEls)
print('\n Source function assertions:\n')
for ast in graphA.src.asts:
    print(ast)
print('\n Target function assertions:\n')
for ast in graphA.trgt.asts:
    print(ast)

#%%
# FOr certain operations such as determining whether two typed directed graphs can be isomorphic, \
# it is important to verify first that the two TDGs are **sort compatible**, in the sense that their \
# underlying vertex and edge carrier sets have the same sorts, and that the source/target maps \
# have the same sort-signatures in both graphs. Compared to a full isomorphism check, this operation \
# merely checks for sort compatibility, which is in particular only a look-up operation on the Python-\
# parts of the data, and thus very efficient as compared to a more in-depth comparison opeeration.
# Let us define another TDG, and then compare a number of TDGs to each other:

vDictB = {'v1':'sortX', 'v2': 'sortX', 'v3': 'sortZ'}                       # the vertex carrier set
eDictB = {'e1':'sortW', 'e2': 'sortT'}                                      # the edge carrier set
stDictB = {'e1':('v1','v2'), 'e2':('v3','v1')}                              # source/target incidence data
graphB = datatypes.Z3TypedDirectedGraph(S, 'gB', vDictB, eDictB, stDictB)

graphA.isSortCompatibleWith(Gempty)     # returns False, as the sorts do not match of course
graphA.isSortCompatibleWith(graphA)     # returns True
graphB.isSortCompatibleWith(graphA)     # returns False

#%%
#
#