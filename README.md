# ReSMT
This is the official repository of the ``ReSMT`` project.

See the [documentation webpage](https://nicolasbehr.gitlab.io/ReSMT) (or the [PDF version](/docs/_build/latex/resmt.pdf) thereof) for further information.
